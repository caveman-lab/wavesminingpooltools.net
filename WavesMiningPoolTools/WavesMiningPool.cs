﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WavesCS;

namespace WavesMiningPoolTools
{
    public partial class WavesMiningPool
    {
        private readonly string name;
        private readonly Node node;
        private readonly PrivateKeyAccount account;
        private readonly bool isApiRateLimitExpected;

        public WavesMiningPool(string seed, string name = "Leasing pool", string apiHost = Node.MainNetHost, bool isApiRateLimitExpected = true)
        {
            this.account = PrivateKeyAccount.CreateFromSeed(seed, Node.MainNetChainId);
            this.name = name;
            this.node = new Node(apiHost, Node.MainNetChainId);
            this.isApiRateLimitExpected = isApiRateLimitExpected;
        }

        private readonly object ApiLock = new object();
        private void ApiWait()
        {
            // Avoid hitting API server rate limit on public nodes
            if (isApiRateLimitExpected)
            {
                System.Threading.Thread.Sleep(500);
            }
        }

        private WavesMiningPoolStatus GetLatestStatus()
        {
            lock (ApiLock)
            {
                var balanceDetail = node.GetObject($"addresses/balance/details/{account.Address}");

                ApiWait();

                var status = new WavesMiningPoolStatus
                {
                    Address = account.Address,
                    RegularBalance = balanceDetail.GetDecimal("regular", Assets.WAVES),
                    AvailableBalance = balanceDetail.GetDecimal("available", Assets.WAVES),
                    GeneratingBalance = balanceDetail.GetDecimal("generating", Assets.WAVES),
                    EffectiveBalance = balanceDetail.GetDecimal("effective", Assets.WAVES),
                    LatestHeight = node.GetObject($"blocks/height").GetInt("height")
                };

                return status;
            }
        }

        private List<WavesMiningPoolLeaserStatus> GetLatestLeaserInfo()
        {
            lock (ApiLock)
            {
                var leases = node.GetObjects($"leasing/active/{account.Address}");
                Dictionary<string, List<WavesMiningPoolLease>> leasers = new Dictionary<string, List<WavesMiningPoolLease>>();
                foreach (var lease in leases)
                {
                    var address = lease.GetString("sender");
                    if (!leasers.ContainsKey(address)) leasers.Add(address, new List<WavesMiningPoolLease>());
                    leasers[address].Add(new WavesMiningPoolLease
                    {
                        Address = address,
                        Amount = lease.GetDecimal("amount", Assets.WAVES),
                        Height = lease.GetInt("height")
                    });
                }
                return leasers.Select(l => new WavesMiningPoolLeaserStatus { Address = l.Key, Leases = l.Value }).ToList();
            }
        }

        private void UpdateHeights(int currentHeight, IEnumerable<WavesMiningPoolLease> leases, int lastPayoutHeight = -1)
        {
            // <{height}, {timestamp}>
            Dictionary<int, long> info = new Dictionary<int, long>();

            lock (ApiLock)
            {
                foreach (var lease in leases)
                {
                    var effectiveHeight = lease.Height + 1000;
                    if (!info.ContainsKey(effectiveHeight))
                    {
                        if (effectiveHeight > currentHeight)
                        {
                            info[effectiveHeight] = -999;
                        }
                        else if (effectiveHeight <= lastPayoutHeight)
                        {
                            info[effectiveHeight] = 0;
                        }
                        else
                        {
                            var heightInfo = node.GetObject($"blocks/headers/at/{lease.Height}");
                            info[effectiveHeight] = heightInfo.GetLong("timestamp");

                            ApiWait();
                        }
                    }
                    lease.EffectiveSince = info[effectiveHeight];
                }
            }
        }

        /// <summary>
        /// Get pool status without leaser info
        /// </summary>
        /// <returns></returns>
        public WavesMiningPoolStatus GetStatus()
        {
            return GetLatestStatus();
        }

        /// <summary>
        /// Get detailed status with leaser info but without payout weights
        /// </summary>
        public WavesMiningPoolStatus GetDetailedStatus()
        {
            var status = GetLatestStatus();

            ApiWait();

            status.Leasers = GetLatestLeaserInfo();

            return status;
        }

        /// <summary>
        /// Get detailed status with leaser info and payout weight
        /// </summary>
        /// <param name="lastPayout">Unix timestamp in milliseconds of the last payout; Invalid value (like -1) for an unweighted payout</param>
        /// <param name="lastPayoutHeight">Block height of last payout</param>
        /// <param name="minLeasingAmount">Minimum leasing amount for a leaser to qualify for payments</param>
        public WavesMiningPoolStatus GetDetailedStatusForPayout(long lastPayout = -1, int lastPayoutHeight = -1, decimal minLeasingAmount = 0)
        {
            var status = GetDetailedStatus();
            long now = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;

            if (lastPayout < 0 || lastPayout >= now)
            {
                status.Leasers.ForEach(l => { l.Weight = l.Amount < minLeasingAmount ? 0 : l.Amount.Value; });
            }
            else
            {
                status.LastPayout = lastPayout;
                UpdateHeights(status.LatestHeight, status.Leasers.SelectMany(l => l.Leases), lastPayoutHeight);
                decimal curPayoutPeriodTotalMilliseconds = now - lastPayout;
                status.Leasers.ForEach(leaser =>
                {
                    leaser.Weight = leaser.Leases.Sum(
                        lease => lease.EffectiveSince < 0 ? 0 : (lease.Amount * (lease.EffectiveSince <= lastPayout ? 1 : (now - lease.EffectiveSince) / curPayoutPeriodTotalMilliseconds))
                    );
                    if (leaser.Weight < minLeasingAmount) leaser.Weight = 0;
                });
            }

            // Update weight to percentage
            var total = status.Leasers.Sum(l => l.Weight);
            status.Leasers.ForEach(l => { l.Weight = l.Weight / total; });

            return status;
        }
    }

    public class WavesMiningPoolStatus
    {
        public string Address { get; set; }
        public decimal RegularBalance { get; set; }
        public decimal AvailableBalance { get; set; }
        public decimal GeneratingBalance { get; set; }
        public decimal EffectiveBalance { get; set; }
        public int LatestHeight { get; set; }

        #region Only for leaser detail
        public List<WavesMiningPoolLeaserStatus> Leasers { get; set; }
        public int? NumberOfLeasers { get => Leasers?.Select(l => l.Address).Distinct().Count(); }
        #endregion

        #region Only for payout
        public long LastPayout { get; set; } = -1;
        #endregion
    }

    public class WavesMiningPoolLeaserStatus
    {
        public string Address { get; set; }
        public List<WavesMiningPoolLease> Leases { get; set; }
        public decimal? Amount { get => Leases?.Sum(l => l.Amount); }

        #region Only for payout
        /// <summary>
        ///  Because leased amount does not take effect immediately, we have to calculate the payout based on the effective time.
        ///  Hence, some leases should only get portional payout if they did not start leasing before the current period.
        /// </summary>
        public decimal Weight { get; set; } = -1;
        #endregion
    }

    public class WavesMiningPoolLease
    {
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public int Height { get; set; }

        #region Only for payout
        /// <summary>
        /// Any leased amount can only start taking effect after 1000 blocks
        /// </summary>
        public long EffectiveSince { get; set; } = -1;
        #endregion
    }
}