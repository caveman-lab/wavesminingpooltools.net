﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WavesCS;

namespace WavesMiningPoolTools
{
    public partial class WavesMiningPool
    {
        public static void ValidateStatusForPayout(WavesMiningPoolStatus status)
        {
            if (status.NumberOfLeasers == null || status.NumberOfLeasers <= 0)
            {
                throw new Exception("Status is invalid for payout: no leasers in the pool.");
            }

            if (status.Leasers.Any(l => l.Weight < 0))
            {
                throw new Exception("Status is invalid for payout: leaser weight is invalid.");
            }

            if (Math.Round(status.Leasers.Sum(l => l.Weight) * 1000000) != 1000000)
            {
                throw new Exception("Status is invalid for payout: leaser weights do not sum up to 1.");
            }
        }

        public void ValidateBalanceForPayout(Dictionary<Asset, decimal> payoutAmounts, int numberOfLeasersToPay)
        {
            if (payoutAmounts.Any(p => p.Value <= 0))
            {
                throw new Exception("Payout amount must be more than 0.");
            }

            var fee = GetEstimatedTransactionFee(numberOfLeasersToPay, payoutAmounts.Count);

            lock (ApiLock)
            {
                var b = node.GetObject($"addresses/balance/details/{account.Address}").GetDecimal("available", Assets.WAVES);
                if (b < fee + (payoutAmounts.ContainsKey(Assets.WAVES) ? payoutAmounts[Assets.WAVES] : 0))
                {
                    throw new Exception("Waves amount is insufficient for payout.");
                }

                if (payoutAmounts.Any(p => p.Key != Assets.WAVES))
                {
                    var ab = node.GetAssetBalances(account.Address);
                    foreach (var p in payoutAmounts.Where(p => p.Key != Assets.WAVES))
                    {
                        var abp = ab.Keys.Where(a => a.Id.Equals(p.Key.Id)).FirstOrDefault();
                        if (abp != null && p.Value <= ab[abp]) continue;
                        else
                        {
                            throw new Exception($"{p.Key.Name} amount is insufficient for payout.");
                        }
                    }
                }
            }
        }

        public static decimal GetEstimatedTransactionFee(int numberOfLeasersToPay, int numOfTokens = 1)
        {
            return (((numberOfLeasersToPay / 100) + 1) * 0.001m + numberOfLeasersToPay * 0.0005m) * numOfTokens;
        }

        private IEnumerable<WavesMiningPoolPayment> ProcessMassTransfer(List<MassTransferItem> transfers, Asset asset, int paidCount)
        {
            lock (ApiLock)
            {
                var tx = new MassTransferTransaction(Node.MainNetChainId, account.PublicKey, asset, transfers, $"{name} payout: {asset.Name} ({paidCount + 1} - {paidCount + transfers.Count})");
                tx.Sign(account);
                var id = node.Broadcast(tx).ParseJsonObject().GetString("id");

                return transfers.Select(t => new WavesMiningPoolPayment
                {
                    Recipient = t.Recipient,
                    TxId = id,
                    Asset = asset.Name,
                    Amount = t.Amount
                });
            }
        }

        public List<WavesMiningPoolPayment> ProcessPayment(WavesMiningPoolStatus status, Asset asset, decimal amount)
        {
            List<WavesMiningPoolPayment> payments = new List<WavesMiningPoolPayment>();

            List<MassTransferItem> transfers = new List<MassTransferItem>();

            int paidCount = 0;

            foreach (var leaser in status.Leasers)
            {
                var amt = Math.Floor((leaser.Weight * amount) * 100000000) / 100000000;
                if (amt > 0)
                {
                    transfers.Add(new MassTransferItem(leaser.Address, amt));
                    if (transfers.Count == 100)
                    {
                        payments.AddRange(ProcessMassTransfer(transfers, asset, paidCount));

                        paidCount += 100;
                        transfers = new List<MassTransferItem>();

                        ApiWait();
                    }
                }
            }

            if (transfers.Count > 0)
            {
                payments.AddRange(ProcessMassTransfer(transfers, asset, paidCount));
            }

            return payments;
        }

        public void ValidatePayment(List<WavesMiningPoolPayment> payments)
        {
            lock (ApiLock)
            {
                Dictionary<string, MassTransferTransaction> txs = new Dictionary<string, MassTransferTransaction>();
                foreach (var payment in payments)
                {
                    if (string.IsNullOrWhiteSpace(payment.TxId))
                    {
                        throw new Exception($"Payment does not have transaction id.");
                    }

                    if (!txs.ContainsKey(payment.TxId))
                    {
                        txs[payment.TxId] = (MassTransferTransaction)node.GetTransactionById(payment.TxId);

                        ApiWait();
                    }

                    if (txs[payment.TxId].Transfers.Any(t => t.Recipient == payment.Recipient && t.Amount == payment.Amount))
                    {
                        // Looks good
                    }
                    else
                    {
                        throw new Exception($"Payment does not match. Address: {payment.Recipient}, Asset: {payment.Asset}, TxId: {payment.TxId}, Amount: {payment.Amount}");
                    }
                }
            }
        }

        private readonly object PayoutLock = new object();
        public WavesMiningPoolPayoutResult Payout(WavesMiningPoolStatus status, Dictionary<Asset, decimal> payoutAmounts)
        {
            lock (PayoutLock)
            {
                var result = new WavesMiningPoolPayoutResult
                {
                    PoolStatus = status,
                    PayoutAmounts = payoutAmounts.Select(p => new KeyValuePair<string, decimal>(p.Key.Name, p.Value)).ToList(),
                    Payments = new List<WavesMiningPoolPayment>()
                };

                try
                {
                    ValidateStatusForPayout(status);

                    var numberOfLeasersToPay = status.Leasers.Where(l => l.Weight > 0).Count();
                    if (payoutAmounts.ContainsKey(Assets.WAVES)) payoutAmounts[Assets.WAVES] -= GetEstimatedTransactionFee(numberOfLeasersToPay, payoutAmounts.Count);
                    ValidateBalanceForPayout(payoutAmounts, numberOfLeasersToPay);

                    ApiWait();

                    // Payment
                    foreach (var amt in payoutAmounts)
                    {
                        result.Payments.AddRange(ProcessPayment(status, amt.Key, amt.Value));
                        ApiWait();
                    }

                    // Validate Result
                    System.Threading.Thread.Sleep(120000);  // Wait for 2 mins for the transactions to be confirmed
                    ValidatePayment(result.Payments);
                }
                catch (Exception ex)
                {
                    result.Error = ex;
                }

                return result;
            }
        }
    }

    public class WavesMiningPoolPayoutResult
    {
        public bool HasError { get => Error != null; }
        public Exception Error { get; set; }
        public List<KeyValuePair<string, decimal>> PayoutAmounts { get; set; }
        public List<WavesMiningPoolPayment> Payments { get; set; }
        public WavesMiningPoolStatus PoolStatus { get; set; }
    }

    public class WavesMiningPoolPayoutSummary
    {
        public List<KeyValuePair<string, decimal>> PayoutAmounts { get; set; }
        public int NumberOfLeasers { get; set; }
    }

    public class WavesMiningPoolPayment
    {
        public string Recipient { get; set; }
        public string TxId { get; set; }
        public string Asset { get; set; }
        public decimal Amount { get; set; }
    }
}