## WavesMiningPoolTools.NET

Useful tools to help you manage your WAVES mining pool

<br/>

### Getting Started

Clone the project and also the WavesCS project: 

https://github.com/wavesplatform/WavesCS

Build WavesCS and add both WavesCS.dll and Newtonsoft.Json.dll to the References of WavesMiningPoolTools project and build.

Add WavesMiningPoolTools.dll to your project's References and in your code as:

`using WavesMiningPoolTools;`

Create a WavesMiningPool object using your pool's seed. Optionally you can use the API service of your own node.

**WAVES seed is sensitive data. Make sure you only use it in a safe working environment.**

`var pool = new WavesMiningPool(YOUR_SEED, OPTIONAL_NAME, OPTIONAL_HOST);`

Then you can use this object to check pool status or make mining rewards payout easily.

<br/>

### Documentation

Get mining pool status:

`var status = pool.GetStatus(); // Basic`

`var status = pool.GetDetailedStatus(); // Including leaser detail`

`var status = pool.GetDetailedStatusForPayout(OPTIONAL_LAST_PAYOUT_TIMESTAMP, OPTIONAL_LAST_PAYOUT_HEIGHT); // Leaser weights are calculated for payout`

Example of rewards payout:

```
var status = pool.GetDetailedStatusForPayout();
var result = pool.Payout(status, new Dictionary<Asset, decimal> { { Assets.WAVES, status.AvailableBalance } });
```


<br/>

#### Prerequisites

Visual Studio 2017+


#### Language

C#


#### Author

Caveman


#### License

This project is licensed under the MIT License - see the LICENSE file for details
